<?php
class View{

    public $template_view;

    function DateTimeFromSQL($data, $format){

    	$data = date_format($data, $format);

    	$enMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    	$rusMonths = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
		return str_replace($enMonths, $rusMonths, $data);
    }
    
    function Generate($content_view, $template_view, $data = null){
    	
        // if(is_array($data)){
        //     extract($data);
        // }
        
        include 'application/views/' . $template_view;
    }
}
