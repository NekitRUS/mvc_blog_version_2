<?php
class Model{

    // function GetData(){
    // }

    function GetConnection(){

    	return Database::GetInstance()->GetConnection();
    }

    function DateTimeForSQL($date){

        return $date->format("Y-m-d H:i:s");
    }
}

class Database{

	private $_connection;
	private static $_instance = null;

	static function GetInstance(){

		if(self::$_instance === null){
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public function GetConnection(){
		
		return $this->_connection;
	}

	private function __construct(){

		global $config;

		$this->_connection = new mysqli($config['DB_HOST'], $config['DB_USER'], $config['DB_PASSWORD']);
        if (mysqli_connect_error()) {
            exit('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }

        $this->_connection->query("CREATE DATABASE IF NOT EXISTS " . $config['DB_NAME'] .
            " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $this->_connection->query("USE " . $config['DB_NAME']);
        $this->_connection->query("SET character_set_client='utf8'");
        $this->_connection->query("SET character_set_connection='utf8'");
        $this->_connection->query("SET character_set_results='utf8'");

        $this->_connection->query("CREATE TABLE IF NOT EXISTS users(
            id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
            login VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci UNIQUE NOT NULL,
            password VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            salt VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            e_mail VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            full_name VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            vk VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci,
            linkedIn VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci,
            twitter VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci,
            gplus VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci,
            avatar VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            details TEXT CHARACTER SET utf8 COLLATE utf8_general_ci
            )
            DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;");

        $this->_connection->query("CREATE TABLE IF NOT EXISTS blogs(
            id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
            user_login VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            date DATETIME NOT NULL,
            caption VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            content LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            FOREIGN KEY (user_login) REFERENCES users(login),
            FULLTEXT KEY titleAndBody (caption, content)
            )
            DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;");

        $this->_connection->query("CREATE TABLE IF NOT EXISTS comments(
            id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
            blog_id INT NOT NULL,
            user_login VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            date DATETIME NOT NULL,
            text TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            FOREIGN KEY (user_login) REFERENCES users(login),
            FOREIGN KEY (blog_id) REFERENCES blogs(id)
            )
            DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;");
	}

	private function __clone(){
    }

    private function __wakeup(){
    }
}