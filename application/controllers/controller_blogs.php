<?php
class Controller_blogs extends Controller{

    function __construct(){

        $this->model = new Model_blogs();
        $this->view = new View();
        //session_id(1); //Local virtual host fix...
        session_start();
    }
    
    function action_index(){

        $this->action_page('1');
    }
    
    function action_add(){

        $captionIsEmpty = false;
        $textIsEmpty = false;
        $textTooLong = false;
        $error = [];
        $usersData = [];
        $post = $_SERVER['REQUEST_METHOD'] == 'POST';
        $logged = isset($_SESSION['login']);
            
        if ($post){
            $usersData = ['caption' => trim($_POST['caption']), 'text' => trim($_POST['text'])];

            if ($logged){
                $usersData['user'] = $_SESSION['login'];
            }
            else{
                $error['logged'] = true;
            }
            
            if ($usersData['caption'] == ''){
                $captionIsEmpty = true;
                $error['caption'] = true;
            }
            if ($usersData['text'] == ''){
                $textIsEmpty = true;
                $error['textEmpty'] = true;
            }
            if (strlen($usersData['text']) > 15000000){
                $textTooLong = true;
                $error['textLong'] = true;
            }

            if ($logged && !$captionIsEmpty && !$textIsEmpty && !$textTooLong){
                $this->model->InsertBlog(['Name' => $usersData['user'], 'Caption' => $usersData['caption'], 'Content' => $usersData['text']]);
                $usersData = [];
                header('Location: /blogs');
                exit;
            }
        }
        
        $data['error'] = $error;
        $data['info'] = $usersData;
        $this->view->Generate('add_view.php', 'template_view.php', $data);
    }

    function action_blog($blogID){

        if(!$this->IsPositiveInteger($blogID)){
            Route::ErrorPage404();
            exit;
        }
        
        $textIsEmpty = false;
        $textTooLong = false;
        $error = [];
        $usersData = [];
        $post = $_SERVER['REQUEST_METHOD'] == 'POST';
        $logged = isset($_SESSION['login']);
            
        if($post){
            $usersData['text'] = trim($_POST['text']); 

            if($logged){
                $usersData['user'] = $_SESSION['login'];
            }
            else{
                $error['logged'] = true;
            }
            
            if($usersData['text'] == ''){
                $textIsEmpty = true;
                $error['textEmpty'] = true;
            }
            if(strlen($usersData['text']) > 65000){
                $textTooLong = true;
                $error['textLong'] = true;
            }

            if($logged && !$textIsEmpty && !$textTooLong){
                $this->model->InsertComment(['blog_id' => $blogID, 'user_login' => $usersData['user'], 'text' => $usersData['text']]);
                $usersData = [];
                header('Refresh:0');
                exit;
            }
        }
        
        $blog = $this->model->GetBlog($blogID);
        if($blog){
            $data['info'] = $blog;
        }
        else{
            Route::ErrorPage404();
            exit;
        }
        $data['error'] = $error;
        $data['input'] = $usersData;
        $this->view->Generate('blog_view.php', 'template_view.php', $data);
    }

    function action_page($number, $async = false){

        if(!$this->IsPositiveInteger($number)){
            Route::ErrorPage404();
            exit;
        }

        $data = $this->model->GetBlogs((int)$number);

        $data['cur_page'] = (int)$number;
        if($data['cur_page'] > $data['total_pages']){
            Route::ErrorPage404();
            exit;
        }

        $data['pagination'] = $this->Pagination($data['cur_page'], $data['total_pages']);

        if($async === 'async'){
            echo json_encode($data);
            exit;
        }
        $this->view->Generate('blogspage_view.php', 'template_view.php', $data);
    }

    function action_search(){

        $data = [];
        $usersData = [];
        $post = $_SERVER['REQUEST_METHOD'] == 'POST';

        if($post){
            $usersData['pattern'] = trim($_POST['pattern']);
            $data = $this->model->Search($usersData);
        }

        $this->view->Generate('search_view.php', 'template_view.php', $data);
    }
}