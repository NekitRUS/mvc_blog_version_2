<?php
class Model_auth extends Model{
    
    public function __construct() {

        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . '/files/avatars')) {
            mkdir($_SERVER['DOCUMENT_ROOT'] . '/files/avatars', 0777, true);
        }
    }
    
    private function GetRandomSalt(){

        $characters = '!@#$%^&*()_+;:?-=0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomSalt = '';
        for ($i = 0; $i < rand(9, 19); $i++) {
            $randomSalt .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomSalt;
    }
     
    private function Encrypt($rawPassword, $salt){

        return md5(strrev(md5($salt . $rawPassword)));
    }
    
    private function RightAddress($str) {

        if(substr($str, 0, 7) == 'http://') {
            $str = str_replace('http://', '', $str);
        }
        if(substr($str, 0, 8) == 'https://'){
            $str = str_replace('https://', '', $str);
        }
        return 'https://' . $str;
    }

    function SendEmail($data){
    
        global $config;
        $name = $data['name'];
        $to = $data['address'];
        $login = $data['login'];
        $password = $data['password'];
        
        $message = file_get_contents($config['EMAIL_TEXT']);
        if(!$message){
        	return false;
        }
        $message = str_replace('{name}', $name, $message);
        $message = str_replace('{login}', $login, $message);
        $message = str_replace('{password}', $password, $message);

        $name = "=?utf-8?B?" . base64_encode($name) . "?=";
        
        $headers   = [];
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-type: text/plain; charset=UTF-8";
        $headers[] = "From: Tiny Blogz co. <noreply@tinyblogz.16mb.com>";
        //$headers[] = "To: =?windows-1251?B?" . base64_encode($name) . "?= <$to>";
        //$headers[] = "From: =?windows-1251?B?" . base64_encode("Tiny Blogz co.") . "?= <noreply@tinyblogz.16mb.com>";
        //$headers[] = "Subject: =?windows-1251?B?" . base64_encode("Регистрация") . "?=";
        
        return mail("$name <$to>", 'Registration', $message, implode(PHP_EOL, $headers));
    }
    
    function GetVerify($data){

        if ($stmt = $this->GetConnection()->prepare("SELECT id FROM users WHERE login = ? OR e_mail = ?")){
            if ($stmt->bind_param('ss', $data['login'], $data['e_mail'])){
                if ($stmt->execute()){
                    if($stmt->store_result()){
                        if($stmt->num_rows > 0){
                            $stmt->free_result();
                            $stmt->close();
                            return true;
                        } 
                    } 
                }
            }
        }
        $stmt->free_result();
        $stmt->close();
        return false; 
    }

    function InsertUser($data){

        global $config;
    	$connection = $this->GetConnection();
        $vk = !empty($data['vk']) ? $this->RightAddress($data['vk']) : null;
        $linkedIn = !empty($data['linkedIn']) ? $this->RightAddress($data['linkedIn']) : null;
        $twitter = !empty($data['twitter']) ? $this->RightAddress($data['twitter']) : null;
        $gplus = !empty($data['gplus']) ? $this->RightAddress($data['gplus']) : null;
        $avatar = !empty($data['avatar']['name']) ? $data['avatar'] : $config['DEFAULT_AVATAR'];
        if (!empty($avatar['name'])){
            $id = $connection->query("SHOW TABLE STATUS WHERE name='users'");
            $id = $id->fetch_array();
            $id = $id['Auto_increment'];
            $fileName = '/files/avatars/' . $id . substr($avatar['name'], strrpos($avatar['name'], '.'));
            if (move_uploaded_file($avatar['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $fileName)){
                $avatar = $fileName;
            }
            else {
                $avatar = $config['DEFAULT_AVATAR'];
            }
        }
        $details = !empty($data['details']) ? $data['details'] : null;
        $salt = $this->GetRandomSalt();
        $password = $this->Encrypt($data['password'], $salt);
        if ($stmt = $connection->prepare("SELECT id FROM users WHERE login = ?")){
            if ($stmt->bind_param('s', $data['login'])){
                if ($stmt->execute()){
                    if($stmt->store_result()){
                        if($stmt->num_rows > 0){
                            $stmt->free_result();
                            $stmt->close();
                            return false;
                        } 
                    } 
                }
            }
        }
        if ($stmt = $connection->prepare("INSERT INTO users (full_name, login, e_mail, password, salt, vk, linkedIn, twitter, gplus, avatar, details)
                                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")){
            if ($stmt->bind_param('sssssssssss',
                                    $data['name'],
                                    $data['login'],
                                    $data['e_mail'],
                                    $password,
                                    $salt,
                                    $vk,
                                    $linkedIn,
                                    $twitter,
                                    $gplus,
                                    $avatar,
                                    $details)){
                $result = $stmt->execute();
                $stmt->close();
                return $result;
            }
        }
        return false;
    }
    
    function GetUser($data){

        if ($stmt = $this->GetConnection()->prepare("SELECT login, password, salt, e_mail, full_name FROM users WHERE login = ?")){
            if ($stmt->bind_param('s', $data['login'])){
                if ($stmt->execute()){
                    if($stmt->bind_result($login, $password, $salt, $e_mail, $full_name)){
                        if($stmt->fetch()){
                            if(($login == $data['login']) && ($password == $this->Encrypt($data['password'], $salt))){
                                $stmt->close();
                                return ['login' => htmlspecialchars($login, ENT_HTML5),
                                        'full_name' => htmlspecialchars($full_name, ENT_HTML5),
                                        'e_mail' => htmlspecialchars($e_mail, ENT_HTML5)];
                            }
                        } 
                    } 
                }
            }
        }
        $stmt->close();
        return false;
    }
}