<script type="text/javascript">pageTitle = 'Поиск | Tiny Blogz';</script>
<section class="card auth">
	<H3>Поиск</H3>
    <form autocomplete="off" action="/blogs/search" method="POST">
        <div class="form">
            <input type="text" name="pattern" placeholder="Что ищем?" value=""/>
        </div>
        <div class="form">
            <input type="submit" value="Найти"/>
        </div>
    </form>
</section>

<div class="container <?php if(!isset($data['blogs'])){echo 'hidden';}?>">
	<section class="card results">
	    <H1>Результаты (<?php echo count($data['blogs']);?>)</H1>
	</section>

    <?php for($i = 0; $i < count($data['blogs']); $i++):
        $num = $data['blogs'][$i]['Number'];?>
        <section class='card blog animation'>
            <H4>
                <div class='number'>
                    #<?php echo $num;?>
                </div>
                <a href=/blogs/blog/<?php echo $num;?>><?php echo nl2br($data['blogs'][$i]['Caption']);?></a>
            </H4>
            <div class='date'>
                <?php echo $this->DateTimeFromSQL($data['blogs'][$i]['Date'], 'j F Y G:i');?>
            </div>
            <div class='author'>
                <strong>@</strong><a class='profile' href=/profiles/profile/<?php echo $data['blogs'][$i]['user_id'];?>><?php echo $data['blogs'][$i]['Name'];?></a>
                [<a href=mailto:<?php echo $data['blogs'][$i]['Email'];?>>E-mail</a>]
            </div>
            <div class='content'>
                <?php echo nl2br($data['blogs'][$i]['Content']);?><br/>
                <a href=/blogs/blog/<?php echo $num;?>>Читать дальше...</a>
            </div>
        </section>
    <?php endfor;?>
</div>