<script type="text/javascript">pageTitle = 'Пользователи | Tiny Blogz';</script>
<section class="card head">
    <H1>Список пользователей</H1>
</section>

<div class='container'>
    <?php for($i = 0; $i < count($data['users']); $i++):?>
        <section class='card animation profileSec'>
            <div>
                <img class='avatar avatar_small' src="<?php echo $data['users'][$i]['avatar'];?>">
                <a class='profile' href=/profiles/profile/<?php echo $data['users'][$i]['id'];?>><?php echo $data['users'][$i]['login'];?></a>
            </div>
        </section>
    <?php endfor;?>
</div>

<section class="card" id="pages">
    <H4 class='large_space' id='pagination'>
        <a href=/profiles/page/1>&#x21E4;</a><?php
        foreach ($data['pagination'] as $value):
            ?><a href=/profiles/page/<?php echo $value?>><?php echo $value?></a><?php
        endforeach
        ?><a href=/profiles/page/<?php echo $data['total_pages']?>>&#x21E5;</a>
    </H4>
</section>
<script type="text/javascript">pageActivate(<?php echo $data['cur_page']?>, 'profiles');</script>