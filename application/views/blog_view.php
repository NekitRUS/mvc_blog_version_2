<?php
    $number = $data['info']['blog']['id'];
    $commentsCount = count($data['info']['comments']);
    $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $title = "Блог на сайте Tiny Blogz";
    //$title = iconv("Windows-1251","UTF-8", $title);
    $title = urlencode($title);
    $description = $data['info']['blog']['caption'];
    //$description = iconv("Windows-1251","UTF-8", $description);
    $description = urlencode($description);
?>
<script type="text/javascript">pageTitle = "Блог №<?php echo $number;?> | Tiny Blogz";</script>
<section class="card blog">
    <H1>#<?php echo $number . " " . nl2br($data['info']['blog']['caption'])?></H1>
    <div class='date'><?php
        echo $this->DateTimeFromSQL($data['info']['blog']['date'], 'j F Y G:i');
    ?></div>
    <div class='author'>
        <strong>@</strong><a class='profile' href=/profiles/profile/<?php
            echo $data['info']['blog']['user_id'];?>><?php echo $data['info']['blog']['user_login'];?>
        </a>
        [<a href=mailto:<?php echo $data['info']['blog']['e_mail'];?>>E-mail</a>]
    </div>
    <div class='content'><?php
        echo nl2br($data['info']['blog']['content']);
    ?></div>
    <div class="share">
        Поделиться: <a class="no_underline" target="_blank"
            title="Опубликовать ссылку в Twitter"
            href="http://twitter.com/share?url=<?php echo $url;?>&text=<?php echo (strlen($description) > 80 ? substr($description . "...", 0, 80) : $description);?>&via=@tinyBlogz">
            <img src="/files/images/twitter32.png">
        </a>
        <a class="no_underline" target="_blank"
            title="Опубликовать ссылку в LinkedIn"
            href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url;?>">
            <img src="/files/images/linkedin32.png">
        </a>
        <a class="no_underline" target="_blank"
            title="Опубликовать ссылку во Вконтакте"
            target="_blank"
            href="http://vk.com/share.php?url=<?php echo $url;?>&title=<?php echo $title;?>&description=<?php echo $description;?>">
            <img src="/files/images/vk32.png">
        </a>
        <a class="no_underline" target="_blank"
            title="Опубликовать ссылку в Google Plus"
            href="https://plus.google.com/share?url=<?php echo $url;?>">
            <img src="/files/images/gplus32.png">
        </a>
    </div>
</section>
    
<section class="card comment">
    <H1>Комментарии (<?php echo $commentsCount;?>)</H1>
</section>

<?php for ($i = $commentsCount - 1; $i >= 0 ; $i--):?>
    <section class='card animation comment'>
        <div class='author'>
            #<?php echo ($i + 1);?>
             <strong>@</strong><a class='profile' href=/profiles/profile/<?php echo $data['info']['comments'][$i]['user_id'];?>><?php
                echo $data['info']['comments'][$i]['user_login'];?>
            </a>
            [<a href=mailto:<?php echo $data['info']['comments'][$i]['e_mail'];?>>E-mail</a>]
        </div>
        <div class='date'><?php
            echo $this->DateTimeFromSQL($data['info']['comments'][$i]['date'], 'j F Y') . ' в ' . 
                $this->DateTimeFromSQL($data['info']['comments'][$i]['date'], 'G:i:s');?>
        </div>
        <div class='content'><?php
            echo nl2br($data['info']['comments'][$i]['text']);?>
        </div>
    </section>
<?php endfor;?>

<section class="card comment">
    <H3>Оставить комментарий</H3>
    <form autocomplete="off" action=<?php echo "'/blogs/blog/" . $data['info']['blog']['id'] . "'";?> method="POST">
        <div class="form">
            <textarea name="text" rows="6" maxlength="65000"><?php
                echo $data["input"]["text"];
            ?></textarea><br/>
        </div>
        <H2 <?php if (isset($_SESSION['login']) || isset($data["error"]["logged"])) { echo "class=hidden"; }?>>
            Только зарегистрированные пользователи могут оставлять комментарии
        </H2>
        <p id="emptyCommentErr" <?php if (!isset($data["error"]["textEmpty"])) { echo "class=hidden"; }?>>Пожалуйста, введите комментарий!</p>
        <p <?php if (!isset($data["error"]["textLong"])) { echo "class=hidden"; }?>>Комментарий должен быть менее 65 000 символов!</p>
        <p <?php if (!isset($data["error"]["logged"])) { echo "class=hidden"; }?>>Для публикации комментария вам необходимо зайти на сайт!</p>
        <div class="form">
            <input type="submit" value="Отправить" onclick="validateComment(event);" />
        </div>
    </form>
</section>