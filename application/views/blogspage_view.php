<script type="text/javascript">pageTitle = 'Блоги | Tiny Blogz';</script>
<section class="card head">
    <H1>Добро пожаловать на Tiny Blogz!</H1>
    <form action="/blogs/search/">
        <div class="form">
            <input type="submit" id="searchBlogLink" value="Найти блоги"/>
        </div>
    </form>
    <form <?php if (!isset($_SESSION['login'])) { echo "class=hidden"; }?> action="/blogs/add/">
        <div class="form">
            <input type="submit" id="addBlogLink" value="Добавить блог"/>
        </div>
    </form>
</section>

<div class='container'>
    <?php for($i = 0; $i < count($data['blogs']); $i++):
        $num = $data['blogs'][$i]['Number'];?>
        <section class='card blog animation'>
            <H4>
                <div class='number'>
                    #<?php echo $num;?>
                </div>
                <a href=/blogs/blog/<?php echo $num;?>><?php echo nl2br($data['blogs'][$i]['Caption']);?></a>
            </H4>
            <div class='date'>
                <?php echo $this->DateTimeFromSQL($data['blogs'][$i]['Date'], 'j F Y G:i');?>
            </div>
            <div class='author'>
                <strong>@</strong><a class='profile' href=/profiles/profile/<?php echo $data['blogs'][$i]['user_id'];?>><?php echo $data['blogs'][$i]['Name'];?></a>
                [<a href=mailto:<?php echo $data['blogs'][$i]['Email'];?>>E-mail</a>]
            </div>
            <div class='content'>
                <?php echo nl2br($data['blogs'][$i]['Content']);?><br/>
                <a href=/blogs/blog/<?php echo $num;?>>Читать дальше...</a>
            </div>
        </section>
    <?php endfor;?>
</div>

<section class="card" id="pages">
    <H4 class='large_space' id='pagination'>
        <a href=/blogs/page/1>&#x21E4;</a><?php
        foreach ($data['pagination'] as $value):
            ?><a href=/blogs/page/<?php echo $value?>><?php echo $value?></a><?php
        endforeach
        ?><a href=/blogs/page/<?php echo $data['total_pages']?>>&#x21E5;</a>
    </H4>
</section>
<script type="text/javascript">pageActivate(<?php echo $data['cur_page']?>, 'blogs');</script>