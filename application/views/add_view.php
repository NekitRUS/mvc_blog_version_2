<script type="text/javascript">pageTitle = 'Новый Блог | Tiny Blogz';</script>
<section class="card add">
    <H3>Добавление блога</H3>
    <form autocomplete="off" action="/blogs/add" method="POST">
        <div class="form">
            <textarea name="caption" rows="1" maxlength="200" placeholder="Название"><?php
                echo $data['info']['caption'];
            ?></textarea><br/>
        </div>
        <p id="emptyCaptionErr" <?php if (!isset($data["error"]["caption"])) { echo "class=hidden"; }?>>
            Пожалуйста, введите название!
        </p>
        <div class="form">
            <textarea name="text" rows="10" maxlength="15000000"><?php
                echo $data["info"]["text"];
            ?></textarea><br/>
        </div>
        <H2 <?php if (isset($_SESSION['login']) || isset($data["error"]["logged"])) { echo "class=hidden"; }?>>
            Только зарегистрированные пользователи могут публиковать блоги
        </H2>
        <p id="emptyBlogErr" <?php if (!isset($data["error"]["textEmpty"])) { echo "class=hidden"; }?>>
            Пожалуйста, введите тело блога!
        </p>
        <p <?php if (!isset($data["error"]["textLong"])) { echo "class=hidden"; }?>>
            Блог должен быть менее 15 000 000 символов!
        </p>
        <p <?php if (!isset($data["error"]["logged"])) { echo "class=hidden"; }?>>
            Для публикации блога вам необходимо зайти на сайт!
        </p>
        <div class="form">
            <input type="submit" value="Разместить блог" onclick="validateBlog(event);" />
        </div> 
    </form>
</section>