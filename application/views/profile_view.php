<script type="text/javascript">pageTitle = "Пользователь <?php echo $data['user']['login'];?> | Tiny Blogz";</script>
<section class="card profileSec">
    <H1><?php echo $data['user']['login'];?></H1><br/>
        <img class='avatar avatar_big' src="<?php echo $data['user']['avatar'];?>">
        <div>
            ФИО: <?php echo $data['user']['full_name'];?>
        </div><br/>
        <div>
            <a href=mailto:<?php echo $data['user']['e_mail'];?>>E-mail</a>
        </div><br/><?php
        if (!empty($data['user']['vk'])) {
            echo "<div><a target='_blank' href=" . $data['user']['vk'] . ">Профиль в VK</a></div><br/>";
        }
        if (!empty($data['user']['linkedIn'])) {
            echo "<div><a target='_blank' href=" . $data['user']['linkedIn'] . ">Профиль в LinkedIn</a></div><br/>";
        }
        if (!empty($data['user']['twitter'])) {
            echo "<div><a target='_blank' href=" . $data['user']['twitter'] . ">Профиль в Twitter</a></div><br/>";
        }
        if (!empty($data['user']['gplus'])) {
            echo "<div><a target='_blank' href=" . $data['user']['gplus'] . ">Профиль в Google+</a></div><br/>";
        }
        if (!empty($data['user']['details'])) {
            echo "<div>О себе: <br/>" . $data['user']['details'] . "</div><br/>";
        }?>
</section>
