<section class="card" id="hello">
    <form action="/auth" method="POST">
    	Здравствуйте, <br/>
    	<strong><?php
    		$started = isset($_SESSION['full_name']);
        	echo $started ? $_SESSION['full_name'] : "Гость";
        ?></strong><br/>
        <div class="form">
        	<input type="submit" id="authLink" value="<?php echo $started ? 'Выйти' : 'Войти';?>"/>
        </div>
    </form>
</section>