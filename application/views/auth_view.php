<script type="text/javascript">pageTitle = 'Авторизация | Tiny Blogz';</script>
<section class="card auth">
	<H3>Войти</H3>
    <form autocomplete="off" action="/auth" method="POST">
        <div class="form">
            <input type="text" name="logLogin" placeholder="Логин" value=""/><br/>
        </div>
        <div class="form">
            <input type="password" name="logPassword" placeholder="Пароль" value=""/><br/>
        </div>
        <p id="loginErr" <?php if (!isset($data['error']['mismatch'])) { echo "class=hidden"; }?>>
            Пожалуйста, проверьте корректность введенных данных
        </p>
        <div class="form">
            <input type="submit" name="log" value="Залогиниться"/>
        </div>
    </form>
</section>

<section class="card auth">
    <H3>Регистрация</H3>
    <form autocomplete="off" enctype="multipart/form-data" action="/auth" method="POST">
        <div class="form">
            <input type="text" name="regLogin" placeholder="Логин*" value="<?php echo $data['info']['regLogin']; ?>"/><br/>
        </div>
        <p id="regLoginErr" <?php if (!isset($data['error']['login'])) { echo "class=hidden"; }?>>Пожалуйста, введите логин</p>
        <div class="form">
            <input type="password" name="regPassword" placeholder="Пароль*" value="<?php echo $data['info']['regPassword']; ?>"/><br/>
        </div>
        <p id="regPasswordErr" <?php if (!isset($data['error']['password'])) { echo "class=hidden"; }?>>Пожалуйста, введите пароль</p>
        <div class="form">
            <input type="text" name="name" placeholder="ФИО*" value="<?php echo $data['info']['name']; ?>"/><br/>
        </div>
        <p id="regNameErr" <?php if (!isset($data['error']['name'])) { echo "class=hidden"; }?>>Пожалуйста, введите ФИО</p>
        <div class="form">
            <input type="text" name="email" placeholder="E-mail*" value="<?php echo $data['info']['email']; ?>"/><br/>
        </div>
        <p id="regEmailErr" <?php if (!isset($data['error']['email'])) { echo "class=hidden"; }?>>Пожалуйста, введите E-mail</p>
        <div class="form">
            <input type="text" name="vk" placeholder="Профиль в VK" value="<?php echo $data['info']['vk']; ?>"/><br/>
        </div>
        <div class="form">
            <input type="text" name="linkedIn" placeholder="Профиль в LinkedIn" value="<?php echo $data['info']['linkedIn']; ?>"/><br/>
        </div>
        <div class="form">
            <input type="text" name="twitter" placeholder="Профиль в Twitter" value="<?php echo $data['info']['twitter']; ?>"/><br/>
        </div>
        <div class="form">
            <input type="text" name="gplus" placeholder="Профиль в Google+" value="<?php echo $data['info']['gplus']; ?>"/><br/>
        </div>
        <input type="file" accept="image/jpeg,image/png,image/jpg" name="avatar_fake" onchange="showPath();"/><br/>
        <div class="form">
            <input type="text" name="avatar" placeholder="Выберите аватар" readonly="readonly" onclick="document.getElementsByName('avatar_fake')[0].click();"/>
        </div>
        <p <?php if (!isset($data['error']['file'])) { echo "class=hidden"; }?> id='fileErr'>
            Пожалуйста, выберите .jpg, .jpeg или .png файл размером не более 700Kb!
        </p>
        <div class="form">
            <textarea name="details" rows="6" maxlength="65000" placeholder="Расскажите немного о себе"><?php
                echo $data["info"]["details"];
            ?></textarea><br/>
        </div>
        <p <?php if (!isset($data['error']['alreadyExists'])) { echo "class=hidden"; }?>>Пользователь с таким логином или такой почтой уже существует!</p> 
        <div class="form">
            <input type="submit" name="reg" value="Зарегистрироваться"/>
        </div>
    </form>
</section>
<script type="text/javascript">validateAuth();</script>