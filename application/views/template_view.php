<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta name="keywords" content="Tiny, blogs, mini, posts">
	    <meta name="description" content="Free Service for Sharing Your Thoughts via Posts and Comments">
	    <meta name="author" content="Titov Nikita">
	    <link rel="stylesheet" type="text/css" href="/css/styles.css" />
	    <script type="text/javascript" src="/js/jquery-2.2.1.min.js"></script>
	    <script type="text/javascript" src="/js/scripts.js"></script>
	    <script type="text/javascript">var pageTitle;</script>
	    <title>Tiny Blogz</title>
	</head>
	<body>
	    <section class="card" id="main">
	    	<a href="/main">На главную</a>
	    </section>
	    <?php 
	    	include 'application/views/greeting.php';
	        include 'application/views/'. $content_view;
	    ?>
	<script type="text/javascript">setTitle(pageTitle);</script>
	</body>
</html>