String.prototype.nl2br = function(){
    return this.replace(/\n/g, "<br />");
}

function setTitle(title){
	if (title !== undefined) {
		document.title = title;
	}
}

function pageActivate(number, path){
	$(document).ready(function(){
		$("#pagination > a").click(function(e){ 
	    	e.preventDefault(); 
	        var posting = $.post($(this).attr('href') + '/async');
	        if(path === 'blogs'){
	        	posting.done(createBlogsPage);
	    	}
	    	else if(path === 'profiles'){
	    		posting.done(createProfilesPage);
	    	}
	        return false;
    	});
		$('.large_space > a:contains("' + number + '")').addClass("active_page no_underline").removeAttr('href');
	});
}

function validateAuth(){
	$(document).ready(function(){
		$("[name='log']").click(function(e){
			if(!$.trim($('[name="logLogin"]').val()) || !$.trim($('[name="logPassword"]').val())){
		    	e.preventDefault(); 
		        $('#loginErr').removeClass('hidden');
	        return false;
	    	}
    	});
		$("[name='logLogin']").on('input', function(){
	        $('#loginErr').addClass('hidden');
    	});
    	$("[name='logPassword']").on('input', function(){
	        $('#loginErr').addClass('hidden');
    	});

    	$("[name='reg']").click(function(e){
			if(!$.trim($('[name="regLogin"]').val())){
		    	e.preventDefault(); 
		        $('#regLoginErr').removeClass('hidden');
	    	}
	    	if(!$.trim($('[name="regPassword"]').val())){
		    	e.preventDefault(); 
		        $('#regPasswordErr').removeClass('hidden');
	    	}
	    	if(!$.trim($('[name="name"]').val())){
		    	e.preventDefault(); 
		        $('#regNameErr').removeClass('hidden');
	    	}
	    	if(!$.trim($('[name="email"]').val()) || !isEmail($('[name="email"]').val())){
		    	e.preventDefault(); 
		        $('#regEmailErr').removeClass('hidden');
		        return false;
	    	}
    	});
		$("[name='regLogin']").on('input', function(){
	        $('#regLoginErr').addClass('hidden');
    	});
    	$("[name='regPassword']").on('input', function(){
	        $('#regPasswordErr').addClass('hidden');
    	});
    	$("[name='name']").on('input', function(){
	        $('#regNameErr').addClass('hidden');
    	});
    	$("[name='email']").on('input', function(){
	        $('#regEmailErr').addClass('hidden');
    	});
	});
}

function isEmail(email){
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

function validateBlog(e){
	$(document).ready(function(){
		if(!$.trim($('[name="text"]').val())){
	    	e.preventDefault();
	        $('#emptyBlogErr').removeClass('hidden');
		}
		if(!$.trim($('[name="caption"]').val())){
	    	e.preventDefault();
	        $('#emptyCaptionErr').removeClass('hidden');
		}
	    $("[name='text']").on('input', function(){
			$('#emptyBlogErr').addClass('hidden');
	    });
	    $("[name='caption']").on('input', function(){
			$('#emptyCaptionErr').addClass('hidden');
	    });
    });
}

function validateComment(e){
	$(document).ready(function(){
		if(!$.trim($('[name="text"]').val())){
	    	e.preventDefault();
	        $('#emptyCommentErr').removeClass('hidden');
    	}
	    $("[name='text']").on('input', function(){
			$('#emptyCommentErr').addClass('hidden');
	    });
    });
}

function createBlogsPage(data){
	var obj = $.parseJSON(data);
	var pagination = $('#pagination');
	pagination.empty();
	$('<a>', {href: '/blogs/page/1', text: '\u21E4'}).appendTo(pagination);
	$.each(obj.pagination, function(i, val){
		$('<a>', {href: '/blogs/page/' + val, text: val}).appendTo(pagination);
	});	
	$('<a>', {href: '/blogs/page/' + obj.total_pages, text: '\u21E5'}).appendTo(pagination);
	pageActivate(obj.cur_page, 'blogs');
	history.pushState(null, null, '/blogs/page/' + obj.cur_page);

	$('.container').empty();
	$.each(obj.blogs, function(i, val){
		var num = val['Number'];
		var section = $('<section>', {class: 'card blog animation'}).appendTo($('.container'));
		var caption = $('<div>', {class: 'number', text: '#' + num}).add('<a>', {href: '/blogs/blog/' + num, html: val['Caption'].nl2br()});
		caption.appendTo($('<H4>').appendTo(section));
		$('<div>', {class: 'date', text: val['Date']['date'].substr(0, val['Date']['date'].lastIndexOf(':'))}).appendTo(section);
		var author = $('<div>', {class: 'author'});
		$('<strong>', {text: '@'}).add('<a>', {class: 'profile', href: '/profiles/profile/' + val['user_id'], text: val['Name']}).appendTo(author);
		author.append('[', $('<a>', {href: 'mailto:' + val['Email'], text: 'E-mail'}), ']');
		author.appendTo(section);
        $('<div>', {class: 'content', html: val['Content'].nl2br()}).append('<br>').append($('<a>', {href: '/blogs/blog/' + num, text: 'Читать дальше...'})).appendTo(section);
	});
}

function createProfilesPage(data){
	var obj = $.parseJSON(data);
	var pagination = $('#pagination');
	pagination.empty();
	$('<a>', {href: '/profiles/page/1', text: '\u21E4'}).appendTo(pagination);
	$.each(obj.pagination, function(i, val){
		$('<a>', {href: '/profiles/page/' + val, text: val}).appendTo(pagination);
	});	
	$('<a>', {href: '/profiles/page/' + obj.total_pages, text: '\u21E5'}).appendTo(pagination);
	pageActivate(obj.cur_page, 'profiles');
	history.pushState(null, null, '/profiles/page/' + obj.cur_page);

	$('.container').empty();
	$.each(obj.users, function(i, val){
		var section = $('<section>', {class: 'card profileSec animation'}).appendTo($('.container'));
		var div = $('<div>');
		div.append($('<img>', {class: 'avatar avatar_small', src: val.avatar}));
		div.append($('<a>', {class: 'profile', href: '/profiles/profile/' + val.id, text: val.login}));
		div.appendTo(section);
	});
}

function showPath(){
	var path = $("[name='avatar_fake']").val();
	$("[name='avatar']").val(path.substr(path.lastIndexOf('\\') + 1));
}